import Ember from 'ember';

export default Ember.Controller.extend({
  location: null,
  getlocation: Ember.computed('location', function () {
    return this.get('location');
  }),
  locationInfo: null,
  currentWeather: null,
  forcastWeather: null,
  actions: {
    getWeather: function () {
      var _this = this;
      //console.log('Not sure whether it come here');
      var result = [];
      return new Ember.RSVP.Promise(function (resolve, reject) {
        Ember.$.ajax({
          type: 'get',
          url: 'http://localhost:3360/' + 'weather',
          data: {location: _this.get('getlocation')},
          success: function (data) {
            Ember.$.each(data[0], function (index, value) {
              var weather = Ember.A();
              if (index === 'location') {
                _this.set('locationInfo', Ember.Object.create(value))
              }
              else if (index === 'current') {
                _this.set('currentWeather', Ember.Object.create(value))

              }
              else {
                _this.set('forcastWeather', Ember.A(value))
              }

            });
            resolve();
          },
          error: function (request, textStatus, error) {
            console.log(error);
            reject(error);
          }
        });
      });
      return result;
    }
  }
});
